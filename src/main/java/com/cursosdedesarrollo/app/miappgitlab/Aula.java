package com.cursosdedesarrollo.app.miappgitlab;

/*
 * Author: David Vaquero <pepesan@gmail.com>
 * License: GPLv3
 * Clase Aula 
 * Campos: 
 * - nombre: String
 */

public class Aula {
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}

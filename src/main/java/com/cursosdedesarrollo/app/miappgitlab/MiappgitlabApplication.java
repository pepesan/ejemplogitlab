package com.cursosdedesarrollo.app.miappgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiappgitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiappgitlabApplication.class, args);
	}

}

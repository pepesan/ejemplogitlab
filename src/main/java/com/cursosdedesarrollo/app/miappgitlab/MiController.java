package com.cursosdedesarrollo.app.miappgitlab;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*
Clase principal del controlador de la ruta /hello
Clase : MiController
Autor: David Vaquero <pepesan@gmail.com>
*/

@RestController
public class MiController {
    @GetMapping("/hello")
    public String index (){
        return "Hola Mundo!";
    }
    @GetMapping("/hello2")
    public String index2 (){
        return "Hola Mundo2!";
    }
    @GetMapping("/hello3")
    public String index3 (){
        return "Hola Mundo3!";
    }
    @GetMapping("/hello4")
    public String index4 (){
        return "Hola Mundo4!";
    }
}
